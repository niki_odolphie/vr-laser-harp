﻿using UnityEngine;
using System.Collections;
using VRTK;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour {

    private LaserHarp myLaserHarp;
    private Keyboard myKeyboard;
    public VRTK_ControllerEvents rightControllerEvents;
    public VRTK_ControllerEvents leftControllerEvents;

    public Text octaveText, beamsText, KeyboardTypeText;

    public AudioClip beep1, beep2, beep3, swish, beamopen;

    public AudioClip simonEnd;

    private AudioSource myAudioSource;

    void Awake()
    {
        myAudioSource = GetComponent<AudioSource>();
    }

    void Start () {
        myLaserHarp = GetComponent<LaserHarp>();
        myKeyboard= GetComponent<Keyboard>();

        rightControllerEvents.TriggerPressed += new ControllerInteractionEventHandler(DoTriggerPressed);
        leftControllerEvents.TriggerPressed += new ControllerInteractionEventHandler(DoTriggerPressed);

        leftControllerEvents.GripPressed += new ControllerInteractionEventHandler(DoGripPressed);
        rightControllerEvents.GripPressed += new ControllerInteractionEventHandler(DoGripPressed);

        rightControllerEvents.ApplicationMenuPressed += new ControllerInteractionEventHandler(DoApplicationMenuPressed);
        leftControllerEvents.ApplicationMenuPressed += new ControllerInteractionEventHandler(DoApplicationMenuPressed);

        octaveText.text = "Octave: " + myKeyboard.startingOctave.ToString();
        beamsText.text = "Beams: " + myLaserHarp.howManyBeams.ToString();
        KeyboardTypeText.text = "Keyboard type: " + "\n"+ myKeyboard.myKeyboardType.ToString();


    }

    void PlayBeep1()
    {
        myAudioSource.clip = beep1;
        myAudioSource.Play();
    }

    void PlayBeep2()
    {
        myAudioSource.clip = beep2;
        myAudioSource.Play();
    }

    void PlayBeep3()
    {
        myAudioSource.clip = beep3;
        myAudioSource.Play();
    }

    void PlaySwish()
    {
        myAudioSource.clip = swish;
        myAudioSource.Play();
    }

    public void PlayBeamOpen()
    {
        myAudioSource.clip = beamopen;
        myAudioSource.Play();
    }

    public void PlaySimonEndGame()
    {
        myAudioSource.clip = simonEnd;
        myAudioSource.Play();
    }


    public void IncreaseBeams()
    {
        Debug.Log("IncreaseBeams");
        myLaserHarp.AddBeam();
        beamsText.text = "Beams: " + myLaserHarp.howManyBeams.ToString();
        myAudioSource.pitch = 1.1f;
        PlayBeep1();
    }

    public void DecreaseBeams()
    {
        Debug.Log("DecreaseBeams");
        myLaserHarp.SubtractBeam();
        beamsText.text = "Beams: " + myLaserHarp.howManyBeams.ToString();
        myAudioSource.pitch = 0.9f;
        PlayBeep1();
    }

    public void DecreaseOctave()
    {
        Debug.Log("DecreaseOctave");
        myKeyboard.DecreaseOctave();
        octaveText.text = "Octave: " + myKeyboard.startingOctave.ToString();
        myAudioSource.pitch = 0.9f;
        PlayBeep2();
    }

    public void IncreaseOctave()
    {
        Debug.Log("IncreaseOctave");
        myKeyboard.IncreaseOctave();
        octaveText.text = "Octave: " + myKeyboard.startingOctave.ToString();
        myAudioSource.pitch = 1.1f;
        PlayBeep2();
    }


    private void DoTriggerPressed(object sender, ControllerInteractionEventArgs e)
    {
        Debug.Log("Trigger pressed");
        myLaserHarp.ResetBeamsButton();
        PlaySwish();

    }

    private void DoGripPressed(object sender, ControllerInteractionEventArgs e)
    {
        Debug.Log("Grip pressed");
        GameObject.Find("SimonGame").GetComponent<SimonGame>().PlaySimon();
    }

    private void DoApplicationMenuPressed(object sender, ControllerInteractionEventArgs e)
    {
        Debug.Log("Menu pressed");
        myKeyboard.CycleKeyboard();
        KeyboardTypeText.text = "Keyboard type: " + "\n" + myKeyboard.myKeyboardType.ToString();
        PlayBeep3();
    }

}
