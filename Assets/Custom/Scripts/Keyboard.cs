﻿using UnityEngine;
using System.Collections;

public class Keyboard : MonoBehaviour {

    public enum KeyboardType { BlackNotes, WhiteNotes, Full};
    public KeyboardType myKeyboardType;

    public enum SynthSounds { Laser, Alternative };
    public SynthSounds currentSynth;

    public int startingOctave = 3;
    public AudioClip[] allNotesArray,blackNotesArray, whiteNotesArray, rendezvousArray;

    private int howManyNotes;
    private int noteOffset;


    void Start()
    {

        //Load all the notes
        LoadLaserHarpSounds();
        BuildArrays();
    }


    public void ToggleSynthSounds()
    {
        
        if (currentSynth == SynthSounds.Alternative)
        {
            Debug.Log("Toggling synth sounds from alternate to laser harp");
            LoadLaserHarpSounds();
        }
        else
        {
            Debug.Log("Toggling synth sounds from laser to alternate");

            LoadAlternativeSynthSounds();
        }

        BuildArrays();
        SetKeyboard();
    }

    private void BuildArrays()
    {
        //Build the black notes array
        int bn = 0;
        for (int i = 0; i < allNotesArray.Length - 1; i++)
        {
            if (allNotesArray[i].name.Contains("#"))
            {
                blackNotesArray[bn] = allNotesArray[i] as AudioClip;
                bn++;
            }
        }

        //Build the white notes array
        int wn = 0;
        for (int i = 0; i < allNotesArray.Length - 1; i++)
        {
            if (!allNotesArray[i].name.Contains("#"))
            {
                whiteNotesArray[wn] = allNotesArray[i] as AudioClip;
                wn++;
            }
        }

    }


    private void LoadLaserHarpSounds()
    {
        allNotesArray = null;
        allNotesArray = Resources.LoadAll<AudioClip>("Laser Harp Sounds");
        currentSynth = SynthSounds.Laser;
    }

    private void LoadAlternativeSynthSounds()
    {
        allNotesArray = null;
        allNotesArray = Resources.LoadAll<AudioClip>("Alternative Synth Sounds");
        currentSynth = SynthSounds.Alternative;
    }

    public void SetKeyboard()
    {

        howManyNotes = GetComponent<LaserHarp>().currentBeams;

        switch (myKeyboardType)
        {
            case KeyboardType.BlackNotes:
                noteOffset = (startingOctave * 5 )-5;
                for (int i = 0; i < howManyNotes; i++)
                    {
                    //Debug.Log(blackNotesArray[i].name);
                        GetComponent<LaserHarp>().beams[i].GetComponent<AudioSource>().clip = blackNotesArray[i + noteOffset]; //TODO Fix This is where the orange beam of death appears
                    GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().startColor = Color.red;
                    GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().endColor = Color.red;
//                    GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().SetColors(Color.red, Color.red);
                        GetComponent<LaserHarp>().beams[i].GetComponent<LaserBeam>().myBeamNumber = i;
                }
                    break;
            case KeyboardType.WhiteNotes:
                noteOffset = (startingOctave * 7) - 7;
                for (int i = 0; i < howManyNotes; i++)
                {
                    GetComponent<LaserHarp>().beams[i].GetComponent<AudioSource>().clip = whiteNotesArray[i + noteOffset];
                    //GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().SetColors(Color.green, Color.green);
                    GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().startColor = Color.green;
                    GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().endColor = Color.green;
                    GetComponent<LaserHarp>().beams[i].GetComponent<LaserBeam>().myBeamNumber = i;
                }
                break;
            case KeyboardType.Full:
                noteOffset = (startingOctave * 12) - 12;
                for (int i = 0; i < howManyNotes; i++)
                {
                    GetComponent<LaserHarp>().beams[i].GetComponent<AudioSource>().clip = allNotesArray[i + noteOffset];
                    if (allNotesArray[i + noteOffset].name.Contains("#"))
                    {
                        //GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().SetColors(Color.red, Color.red);
                        GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().startColor = Color.red;
                        GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().endColor = Color.red;
                    } else
                    {
                        //GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().SetColors(Color.green, Color.green);
                        GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().startColor = Color.green;
                        GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().endColor = Color.green;
                    }

                    GetComponent<LaserHarp>().beams[i].GetComponent<LaserBeam>().myBeamNumber = i;
                }
                break;
            //case KeyboardType.Rendezvous:
            //    if (startingOctave == 1)
            //    {

            //    }
            //    else if (startingOctave == 2)
            //    {

            //    }
            //    else if (startingOctave == 3)
            //    {

            //    }
            //    break;
        }

        for (int i = 0; i < howManyNotes; i++)
        {
            if (GetComponent<LaserHarp>().beams[i].GetComponent<AudioSource>().clip == null)
            {
                //GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().SetColors(Color.blue, Color.blue);
                GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().startColor = Color.blue;
                GetComponent<LaserHarp>().beams[i].GetComponent<LineRenderer>().endColor = Color.blue;

            }
        }

    }


    public void IncreaseOctave()
    {
        if (startingOctave + 1 > 4)
        {
            startingOctave = 4;
        }
        else
        {
            startingOctave++;
        }
    }

    public void DecreaseOctave()
    {
        if (startingOctave - 1 < 1)
        {
            startingOctave = 1;
        }
        else
        {
            startingOctave--;
        }
    }

    public void CycleKeyboard()
    {
   


        switch (myKeyboardType)
        {
            case KeyboardType.BlackNotes:
                myKeyboardType = KeyboardType.WhiteNotes;
                break;
            case KeyboardType.WhiteNotes:
                myKeyboardType = KeyboardType.Full;
                break;
            case KeyboardType.Full:
                myKeyboardType = KeyboardType.BlackNotes;
                break;
            //case KeyboardType.Rendezvous:
            //    myKeyboardType = KeyboardType.BlackNotes;
            //    break;
        }
    }

}
