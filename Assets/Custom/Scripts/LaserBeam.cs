﻿
//This is free to use and no attribution is required
//No warranty is implied or given
using UnityEngine;
using System.Collections;
using VRTK;

[RequireComponent(typeof(LineRenderer))]

public class LaserBeam : MonoBehaviour
{

    public float laserWidth = 1.0f;

    public int myBeamNumber; //Used for Simon game

    public GameObject myTarget;

    public GameObject laserHitPointLight;
    private GameObject hitLight;

    private Renderer rend;

    private bool autoPlayMode;

    private string myAudioClip;

    private AudioSource myAudioSource;

    Color originalStartColor;
    Color originalEndColor;

    LineRenderer lineRenderer;
    int length;
    //Cache any transforms here
    Transform myTransform;
    Transform endEffectTransform;

    //private LaserHarp laserHarp;
    private SimonGame simonGame;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        myAudioSource = GetComponent<AudioSource>();
        //        lineRenderer.SetWidth(laserWidth, laserWidth);
        lineRenderer.startWidth = laserWidth;
        lineRenderer.endWidth = laserWidth;
        myTransform = transform;

        rend = GetComponent<Renderer>();
        //laserHarp = GameObject.Find("LaserHarp").GetComponent<LaserHarp>();
        simonGame = GameObject.Find("SimonGame").GetComponent<SimonGame>();

        autoPlayMode = false;

        originalStartColor = lineRenderer.startColor;
        originalEndColor = lineRenderer.endColor;

    }

    void Update()
    {
        UpdateLength();

        float fadeRandom = Random.Range(0.5f, 1f);
        rend.material.SetFloat("_FadeLevel", fadeRandom);

        float coreRandom = Random.Range(0.5f, 1f);
        rend.material.SetFloat("_CoreCoef", coreRandom);

    }

    void UpdateLength()
    {
        RaycastHit[] hit;
        hit = Physics.RaycastAll(myTransform.position, myTarget.transform.position, Mathf.Infinity);

        int i = 0;
        while (i < hit.Length)
        {
            if (!hit[i].collider.isTrigger)
            {


                lineRenderer.SetPosition(1, hit[i].point);

                if (hitLight == null)
                {
                    if (myAudioSource.clip.name != null)
                    {
                        myAudioClip = myAudioSource.clip.name;
                        //if (myAudioClip != null)
                        //{
                        //hitLight = Instantiate(laserHitPointLight);
                        hitLight = SmartPool.Spawn("Laser point lights");
                        SetLightColour();

                        

                        //Debug.Log("hit i is: " + hit[i].transform.name);
                        //hit[i].transform.gameObject.GetComponent<HandController>().Vibrate();
                        //VRTK_ControllerActions myHand = hit[i].transform.gameObject.GetComponent<HandController>().vibrate();
                        //VRTK_ControllerActions vrtkController = hit[i].transform.gameObject.GetComponent<VRTK_ControllerActions>();
                        //Debug.Log(hit[i]);
                        //vrtkController.TriggerHapticPulse((ushort)1f, 0.5f, 0.01f);

                        //}
                    }
                }
                if (hitLight != null)
                {
                    hitLight.transform.position = new Vector3(hit[i].point.x, hit[i].point.y - 0.02f, hit[i].point.z);
                }
                
                if (!GetComponent<AudioSource>().isPlaying)
                {
                    GetComponent<AudioSource>().Play();
                    
                }

                return;
            }
            i++;
        }

        //If we're not hitting anything, don't play the line effects and draw at normal length
        lineRenderer.SetPosition(1, myTarget.transform.position); 
        //DestroyImmediate(hitLight);
        
        
        if (GetComponent<AudioSource>().isPlaying && !autoPlayMode)
        {
            GetComponent<AudioSource>().Stop();
            simonGame.BeamHit(myBeamNumber); //Put here because this is only triggered once per sound
            SmartPool.Despawn(hitLight);
        }
    }

    public void AutoPlayMySound(float duration)
    {
        autoPlayMode = true;
        //lineRenderer.SetColors(Color.white, Color.white);
        lineRenderer.startColor = Color.white;
        lineRenderer.endColor = Color.white;
        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;

        if (!GetComponent<AudioSource>().isPlaying && autoPlayMode)
        {
            GetComponent<AudioSource>().Play();
            StartCoroutine(StopPlayingAfterDelay(duration));
        }
    }

    IEnumerator StopPlayingAfterDelay(float time)
    {
        yield return new WaitForSeconds(time);
        GetComponent<AudioSource>().Stop();
        lineRenderer.startColor = originalStartColor;
        lineRenderer.endColor = originalEndColor;
        lineRenderer.startWidth = 0.05f;
        lineRenderer.endWidth = 0.05f;
        autoPlayMode = false;
    }

    void SetLightColour()
    {
        if (hitLight != null)
        {
            if (myAudioClip.Contains("#"))
            {
                hitLight.GetComponent<Light>().color = Color.red;
            }
            else
            {
                hitLight.GetComponent<Light>().color = Color.green;


            }
        }
    }

}