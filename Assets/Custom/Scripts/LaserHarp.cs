﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LaserHarp : MonoBehaviour {

    [Range(1, 13)]
    public int howManyBeams = 6;
    public int currentBeams;
    public GameObject beamPrefab;
    public GameObject leftEdge, rightEdge;
    private float leftEdgeStartPos, rightEdgeStartPos;

    public GameObject[] beams;
    public GameObject[] targets;

    private float lerpValue;
    
    void Awake()
    {
        leftEdgeStartPos = leftEdge.transform.position.x;
        rightEdgeStartPos = rightEdge.transform.position.x;
    }

    void Start () {
        CloseTheBeams(0.0f, 0f);
        SetupTheBeams();
        //        OpenTheBeams(1f, 1f);
        GetComponent<ButtonController>().PlayBeamOpen();
        StartCoroutine(ResetBeams(3.0f, 0f));

    }


    void Update() { //Done here so can do the opening animation by seperating the points

            lerpValue = 1f / currentBeams;
            for (int i = 0; i < currentBeams; i++)
            {
                Vector3 pos = Vector3.Lerp(leftEdge.transform.position, rightEdge.transform.position, lerpValue);
                lerpValue = lerpValue + 1f / currentBeams;
                targets[i].transform.position = pos;
                beams[i].GetComponent<LineRenderer>().SetPosition(1, targets[i].transform.position);
                beams[i].GetComponent<LaserBeam>().myTarget = targets[i];

            }

    }

    void SetupTheBeams()
    {
        foreach(GameObject beam in beams)
        {
            Destroy(beam);
        }
        beams = null;

        foreach (GameObject target in targets)
        {
            Destroy(target);
        }
        targets = null;

        currentBeams = howManyBeams;

        beams = new GameObject[currentBeams];
        targets = new GameObject[currentBeams];

        for (int i = 0; i < currentBeams; i++)
        {
            // Create the beams
            GameObject go = Instantiate(beamPrefab,
                new Vector3(0, 0, 0),
                Quaternion.Euler(0, 0, 0),
                gameObject.transform) as GameObject;
            beams[i] = go;

            //Create the targets
            targets[i] = new GameObject("target " + i);
        }

        GetComponent<Keyboard>().SetKeyboard();
    }


    void CloseTheBeams(float duration, float delay)
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(leftEdge.transform.DOMoveX(0f, duration).SetDelay(delay));
        mySequence.Insert(0,rightEdge.transform.DOMoveX(0f, duration).SetDelay(delay));
        mySequence.Play();
    }

    void OpenTheBeams(float duration, float delay)
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(leftEdge.transform.DOMoveX(leftEdgeStartPos, duration).SetDelay(delay));
        mySequence.Insert(0, rightEdge.transform.DOMoveX(rightEdgeStartPos, duration).SetDelay(delay));
        mySequence.Play();
    }

    IEnumerator ResetBeams(float duration, float delay)
    {
        CloseTheBeams(duration, delay);
        yield return new WaitForSeconds(duration + delay);
        SetupTheBeams();
        OpenTheBeams(duration, delay);
        yield return new WaitForSeconds(duration + delay);
    }

    public void ResetBeamsButton()
    {
        StartCoroutine(ResetBeams(0.25f,0f));
    }

    public void AddBeam()
    {
        if(howManyBeams +1 > 26)
        {
            howManyBeams = 26;
        }
        else
        {
            howManyBeams++;
        }
    }

    public void SubtractBeam()
    {
        if(howManyBeams-1 < 6)
        {
            howManyBeams = 6;
        }
        else
        {
            howManyBeams--;
        }
    }

}
