﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SimonGame : MonoBehaviour
{

    public LaserHarp laserHarp;
    //int simonScore = 0;
    //int simonHighScore = 0;
    int humanPosition = 0;
    //int currentSimonGameLength = 0;

    int newNote;
    int lastBeamHitByHuman;

    bool playingSimon;

    private List<int> simonGame = new List<int>();
    public List<Light> lights = new List<Light>();
    public List<FoggyLight> foggyLights = new List<FoggyLight>();

    public Color[] originalLightColours,originalFoggyLightColours;

    void Update()
    {
        if (Input.GetKeyDown("s"))
        {
            if (playingSimon == false)
            {
                //Debug.Log("Starting Simon game");
                PlaySimon();
            }
        }

        if (Input.GetKeyDown("r"))
        {
            PlayerPrefs.DeleteAll();
            CheckSavedScoresForFirstRun();
        }

    }


    void Start()
    {
        ResetGame();
        originalLightColours = new Color[lights.Count];
        originalFoggyLightColours = new Color[foggyLights.Count];

        for (int i = 0; i < lights.Count; i++)
        {
            originalLightColours[i] = lights[i].color;
        }

        for (int i = 0; i < foggyLights.Count; i++)
        {
            originalFoggyLightColours[i] = foggyLights[i].PointLightColor;
        }

    }

    public void PlaySimon()
    {
        if (!playingSimon)
        {
            //Debug.Log("Starting Simon game");
            ResetGame();
            playingSimon = true;
            DimTheLights(); //TODO add fade effect
            Invoke("SimonComputerPlays", 2f);
            //Play a start sound
            CheckSavedScoresForFirstRun();
        }
        else
        {
            //Debug.Log("Continue simon game");
            //SimonComputerPlays();
        }

    }

    void DimTheLights()
    {
        for (int i = 0; i < lights.Count; i++)
        {
            lights[i].color = Color.blue;
        }

        for (int i = 0; i < foggyLights.Count; i++)
        {
            foggyLights[i].PointLightColor = Color.blue;
        }
    }

    void ResetTheLights()
    {
        for (int i = 0; i < lights.Count; i++)
        {
            lights[i].color = originalLightColours[i];
        }

        for (int i = 0; i < foggyLights.Count; i++)
        {
            foggyLights[i].PointLightColor = originalFoggyLightColours[i];
        }
    }

    void ResetGame()
    {
        //simonScore = 0;
        humanPosition = 0;
        playingSimon = false;

        simonGame.Clear();
        CheckSavedScoresForFirstRun();
    }

    void SimonComputerPlays()
    {
        newNote = Random.Range(0, laserHarp.beams.Length);
        simonGame.Add(newNote);
        //Debug.Log("Newnote; " + newNote);

        //Debug.Log("Game length: " + simonGame.Count);

        StartCoroutine(ComputerPlaysTheirNotes());

        //SimonHumanPlays();
        //humanPosition = 0;

    }

    IEnumerator ComputerPlaysTheirNotes()
    {
        for (int i = 0; i < simonGame.Count; i++)
        {
            laserHarp.beams[simonGame[i]].GetComponent<LaserBeam>().AutoPlayMySound(0.7f);
            yield return new WaitForSeconds(1.0f);
        }



    }

    void SimonHumanPlays()
    {

        Debug.Log("The human played: " + lastBeamHitByHuman + ".  They should hit: " + simonGame[humanPosition]);
        Debug.Log("Human played sequence: " + humanPosition + " of a game length of: " + (simonGame.Count -1));
        Debug.Log("humanPosition:" + humanPosition);
        Debug.Log("    Count:    " + simonGame.Count);

        //if (lastBeamHitByHuman == (int)simonGame[humanPosition])
        if (simonGame[humanPosition].Equals(lastBeamHitByHuman))
            {
            //Debug.Log("Correct Note");
            humanPosition++;
            UpdateScore(humanPosition);

            if (humanPosition  == simonGame.Count)
            {
               // Debug.Log("player hit the end of the pattern, hand over to the computer");
                humanPosition = 0;
                //Debug.Log("Human Position after reset:" + humanPosition);
                Invoke("SimonComputerPlays",1f);  
            }
               


        }
        else
        {
            //Debug.Log("Incorrect Note");
            SimonEndGame();
            laserHarp.GetComponent<ButtonController>().PlaySimonEndGame();
            //UpdateScore(0);
            //humanCurrentScore.text = newScore.ToString();
        }
    }

    void SimonEndGame()
    {
        ResetGame();
        ResetTheLights();
        laserHarp.ResetBeamsButton();
    }

    public void BeamHit(int beamNumber)
    {
        if (playingSimon)
        {
            //Debug.Log("The human played beam: " + beamNumber);
            lastBeamHitByHuman = beamNumber;
            SimonHumanPlays();
        }

    }

    #region Score management

    public Text simonTopScoreText, humanCurrentScore;
    int savedScore;
    bool neverRan;

    void CheckSavedScoresForFirstRun()
    {
        LoadScore();
        //if (savedScore == 0) {
        //    neverRan = true;
        //} else
        //{
        //    neverRan = false;
        //    UpdateScore(savedScore);
        //}
    }

    void LoadScore()
    {
        savedScore = PlayerPrefs.GetInt("PlayerTopScore");
        simonTopScoreText.text = savedScore.ToString();
    }

    void SaveScore(int newScore)
    {
        PlayerPrefs.SetInt("PlayerTopScore", newScore);
    }

    void UpdateScore(int newScore)
    {
        humanCurrentScore.text = newScore.ToString();

        if (humanPosition > savedScore) //If you've exceeded the top score update the saved top score and the one on display
        {
            SaveScore(humanPosition);
            simonTopScoreText.text = humanPosition.ToString();
        }
    }
    
    #endregion
}
