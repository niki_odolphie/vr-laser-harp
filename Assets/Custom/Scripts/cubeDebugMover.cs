﻿using UnityEngine;
using System.Collections;

public class cubeDebugMover : MonoBehaviour {

    GameObject leftEdge, rightEdge;
    float startY;

	// Use this for initialization
	void Start () {
        leftEdge = GameObject.Find("Left Edge");
        rightEdge = GameObject.Find("Right Edge");
        startY = gameObject.transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(new Vector3(leftEdge.transform.position.x, startY, leftEdge.transform.position.z),
                                            new Vector3(rightEdge.transform.position.x, startY, rightEdge.transform.position.z),
        Mathf.SmoothStep(0f, 1f,
         Mathf.PingPong(Time.time / 8f, 1f)
    ));
    }
}
